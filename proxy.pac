function FindProxyForURL(url, host) {
    if (shExpMatch(host, "*.bva.in.bund.de") || shExpMatch(host, "*risprepository*")) {
        return "PROXY localhost:8888; DIRECT";
    }
    // Everything else directly!
    return "DIRECT";
}
