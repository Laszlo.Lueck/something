@echo off
set /p dsid="Enter DSID: "

docker run -it --rm --privileged ^
 --add-host xray.artifactory.zssi.ivbb.bund.de:193.24.162.8 ^
 --add-host risprepository:172.26.47.249 ^
 --add-host git.zssi.ivbb.bund.de:193.24.162.7 ^
 --add-host jira.zssi.ivbb.bund.de:193.24.162.8 ^
 --add-host wiki.zssi.ivbb.bund.de:193.24.162.9 ^
 --add-host artifactory.zssi.ivbb.bund.de:193.24.162.8 ^
 --add-host chat.zssi.ivbb.bund.de:193.24.162.7 ^
 -e DSID=%dsid% -e OPENCONNECT_URL=https://vpn.msg-systems.com ^
 -p 8888:8888 -p 8889:8889 ^
 t40mas/openconnect-proxy-juniper:latest
