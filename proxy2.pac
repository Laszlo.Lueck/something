function FindProxyForURL(url, host) {
    if (shExpMatch(host, "*.zssi.ivbb.bund.de") || shExpMatch(host, "*risprepository*")) {
        return "PROXY 192.168.0.16:8888; DIRECT";
    }
    // Everything else directly!
    return "DIRECT";
}
